<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Conway!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Hangyaszimulációk</title>
        <para>
            Írj Qt C++-ban egy hangyaszimulációs programot, a forrásaidról utólag reverse engineering jelleggel
            készíts UML osztálydiagramot is!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/10/10/myrmecologist">https://bhaxor.blog.hu/2018/10/10/myrmecologist</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/pocsimate/bhax/tree/master/thematic_tutorials/bhax_textbook/source/7.fejezet/mym"></link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>  
        <para>
            
        
            Ezzel a programmal a hangyák kommunikációját szimuláljuk, amely a feromonokon alapszik.
            A hangya az erősebb feromonszint felé indul el, most is ez fog történni, (mégpedig a cellákra osztott képernyőn)
            megkeresi a legnagyobb feromonszinttel rendelkező szomszédját a cellában.
            Lényeges, hogy a program futása során a cellák feromon-értékei változnak, pontosabban 
            csökkennek, viszont mihelyst hangya lép az adott céllába, megnő.
        </para>
        <para>
           A program futtatásának egyik feltétele, hogy a fájlok ugyanazon mappában helyezkedjenek el.
           A másik feltétel pedig a Qt. Itt lehetséges a teljes, nyílt forrású szoftver használata, ott 
            projekt megnyitásával, majd futtatásával érhető el. Elég csupán a szükséges csomag telepítése
            terminálon keresztül.
            <screen>
            sudo apt install libqt4-dev
            sudo apt install qt5-default    
            </screen>
            Ebben az esetben a szükséges parancsok:
            <programlisting>
            qmake myrmecologist.pro
            make
            ./myrmecologist //-w 250 -m 150 -n 400 -t 10 -p 5 -f 80 -d 0 -a 255 -i 3 -s 3  -c 22 
            </programlisting>
            
            <mediaobject>
          <imageobject>
            <imagedata fileref="source/kapcsolokkal.png" format="PNG"/>
          </imageobject>
        </mediaobject> 
        
            
            <mediaobject>
          <imageobject>
            <imagedata fileref="source/myrmecologist.png" format="PNG"/>
          </imageobject>
        </mediaobject> 
            
        Ez utóbbit a kapcsolók használata nélküli futtatás eredményezte.
            Most, hogy futtattuk, nézzük a forrást. A forrásunkat több fájl fogja alkotni, több osztály, stb.
            Ezek szemléltesére, valamint a megértés könnyítése érdekében készítettem egy UML diagramot, amely 
            szemlélteti egyes fájlok tartalmát, az osztályokat és tagjaikat.
            Nagyobb projektek ezt megkívánják, az áttekinthetőséget, illetve a tervezést is nagyban megkönnyítni.
            
            <mediaobject>
          <imageobject>
            <imagedata fileref="source/ANTUML.png" format="PNG"/>
          </imageobject>
        </mediaobject> 
        
        
        </para> 
        <para>
              A legkisebb osztály az <function>Ant</function>,vagyis hangya osztály, amelyet az 
            <filename>ant.h</filename> header fájl tartalmaz. Leolvashatóak az ábráról a tagjai az osztálynak, + jelet használunk
            a publikus változók, tagfüggvények szemléltetésére.
            
            <programlisting language="c++"><![CDATA[
class Ant
{

public:
    int x;
    int y;
    int dir;

    Ant(int x, int y): x(x), y(y) {
        
        dir = qrand() % 8;
        
    }

};

typedef std::vector<Ant> Ants;]]>
            </programlisting>
            Ebből derül ki, hogy egy hangyának van kezdeti pozíciója, ezt a <varname>x</varname> ,
             illetve <varname>y</varname> koordinátarendszer-beli pontok határozzák meg.
             Ezenfelül a <varname>dir</varname> változó fogja tárolni a hangyánk irányát.
             A <function>typedef</function> működését már bemutattam, itt egyértelműen az történik, hogy 
             <type>Ants</type> típusjelöléssel ellátott vektort készítünk az objektumból. 
        </para>
        <para> 
            Következzen az <filename>antthread.h</filename> header.   

<programlisting language="c++"><![CDATA[
#ifndef ANTTHREAD_H
#define ANTTHREAD_H

#include <QThread>
#include "ant.h"

class AntThread : public QThread
{
    Q_OBJECT

public:
    AntThread(Ants * ants, int ***grids, int width, int height,
             int delay, int numAnts, int pheromone, int nbrPheromone, 
             int evaporation, int min, int max, int cellAntMax);
    
    ~AntThread();
    
    void run();
    void finish()
    {
        running = false;
    }

    void pause()
    {
        paused = !paused;
    }

    bool isRunnung()
    {
        return running;
    }

private:
    bool running {true};
    bool paused {false};
    Ants* ants;
    int** numAntsinCells;
    int min, max;
    int cellAntMax;
    int pheromone;
    int evaporation;
    int nbrPheromone;
    int ***grids;
    int width;
    int height;
    int gridIdx;
    int delay;
    
    void timeDevel();

    int newDir(int sor, int oszlop, int vsor, int voszlop);
    void detDirs(int irany, int& ifrom, int& ito, int& jfrom, int& jto );
    int moveAnts(int **grid, int row, int col, int& retrow, int& retcol, int);
    double sumNbhs(int **grid, int row, int col, int);
    void setPheromone(int **grid, int row, int col);

signals:
    void step ( const int &);

};

#endif
]]></programlisting>
        Látható hogy az <filename>ant.h</filename>-t már igénybe is vesszük, sőt ezen kívül még a 
        <filename>Qthread</filename> osztányt hiszen fogjuk az itt megírt <function>exec()</function>, 
        <function>start()</function> és még számos függvényét használni.
        Az AntThread osztály örökli a QThread osztályt, (másképp: Az AntThread osztály a QThread osztály leszármazottja)
        beleértve a QThread osztály függvényeit, változóit.
        A Q_OBJECT makróra szükség van ahhoz, hogy a Qt jeleit saját eljárással tudjuk kezelni.
        A szóban forgó osztály fogja biztosítani a szálat, amin a hangyák számításai fognak folyni, látható a konstruktorban, hogy
         valamennyi paramétert meg is fog kapni. A publikus rész függvényeit nem okoz gondot értelmezni, a privát részben láthatunk 
         néhány függvényprototípust, illetve egy signal-t is.
         </para>
         <para>
          Következőnek vizsgáljuk meg az <filename>antwin.h</filename> headert.
<programlisting language="c++"><![CDATA[
#ifndef ANTWIN_H
#define ANTWIN_H

#include <QMainWindow>
#include <QPainter>
#include <QString>
#include <QCloseEvent>
#include "antthread.h"
#include "ant.h"

class AntWin : public QMainWindow
{
    Q_OBJECT

public:
    AntWin(int width = 100, int height = 75,
           int delay = 120, int numAnts = 100,
           int pheromone = 10, int nbhPheromon = 3,
           int evaporation = 2, int cellDef = 1,
           int min = 2, int max = 50,
           int cellAntMax = 4, QWidget *parent = 0);

    AntThread* antThread;

    void closeEvent ( QCloseEvent *event ) {

        antThread->finish();
        antThread->wait();
        event->accept();
    }

    void keyPressEvent ( QKeyEvent *event )
    {

        if ( event->key() == Qt::Key_P ) {
            antThread->pause();
        } else if ( event->key() == Qt::Key_Q
                    ||  event->key() == Qt::Key_Escape ) {
            close();
        }

    }

    virtual ~AntWin();
    void paintEvent(QPaintEvent*);

private:

    int ***grids;
    int **grid;
    int gridIdx;
    int cellWidth;
    int cellHeight;
    int width;
    int height;
    int max;
    int min;
    Ants* ants;

public slots :
    void step ( const int &);

};

#endif    
    
    
]]></programlisting>
        Itt már a szimulációnk ablakáról beszélünk.
        Látható is, hogy a QMainWindow osztály leszármazottja lesz az osztályunk.
        A QPainter osztályra a képernyőre való rajzolás miatt lesz szükség, 
         QCloseEvent-re pedig a kilépéskezelés céljából, a jelet pedig a saját 
         jelkezelő függvényünkkel fogjuk kezelni, ez a closeEvent függvény.
         A keyPressEvent függvény nem jelent újdonságot, már találkoztunk korábbi feladatnál vele,
          itt egyes billentyűk leütésének a jelét fogjuk kezelni, jelen esetben: P, a számítások leállítására valamint
           Q, amely hatására kilépünk az ablakból.
           A virtuális destruktorunk fogja biztosítani, hogy a leszármazott osztály példánya megfelelően kerül
            "megsemmisítésre".
        </para>    
        <para>
            Az antwin.cpp és az antthread.cpp összesen több mint 500 sor 
            terjedelmű forráskódot jelent, ezen részeknek függvényeit tárgyalnám, és 
            elemezném mi is történik.  
        </para>    
        <para>
            Az <filename>antthread.cpp</filename>
        </para>    
        <para>
           A konstruktor szokásosan a megfelelő változókat ellátja a hozzá tartozó értékekkel, majd
            a hangyák elhelyezésre kerülnek a rácson.
            A destruktor meghívódása fogja eredményezni a hangyák elpusztítását, illetve a tömböt is megsemmisítjük, amelyben el voltak helyezve.
        </para>    
        <para>
           A <function>sumNbhs()</function> fogja visszadani az adott cella szomszédainak számát.
           A <function>newDir()</function> fogja végezni az új irány adását a hangyáknak, amennyiben ez szükséges.
           A <function>detDirs()</function> függvény fogja megadni, milyen irányba haladhat a hangya. Ehhez az aktuális irányt fogja 
           használni.
           A <function>moveAnts()</function> értelemszerűen az aktuális hangya mozgatásáért lesz felelős.
           A <function>timeDevel()</function> fog valamennyi hangyát mozgatni, megfigyelhető, hogy két rácsot használ ehhez, egy régit és egy újat.
           A <function>setPheromone()</function> fogja az adott cella feromon-értékét beállítani.
           Végezetül a <function>run()</function> függvény, ez fogja a folyamatos futást biztosítani. A <function>start</function> fogja kikényszeríteni a meghívódását.
        </para>    
        <para>
          Az <filename>antwin.cpp</filename>  
        </para>    
        <para>
            Konstruktora beállítja az ablak értékeit (magasság, szélesség ...), beállítja
             az ablak tetején látható címet.
            Létrehozzuk a rácsokat, a <varname>gridIdx</varname> változó értékének megfelelő rácspontot feltöltjük.
            A <function>painEvent()</function> festi ki az ablakunkat, ellátja a hangyákat, illetve a feromon tartalmú rácsokat színnel.
            A destruktor fogja a rácsokat, a szálakat, illetve a hangyákat megsemmisíteni.
        </para>    
        <para>
            A <filename>main.cpp</filename> 
        </para>    
        <para>
          <programlisting language="c++"><![CDATA[
#include <QApplication>
#include <QDesktopWidget>
#include <QDebug>
#include <QDateTime>
#include <QCommandLineOption>
#include <QCommandLineParser>

#include "antwin.h"

/*
 * 
 * ./myrmecologist -w 250 -m 150 -n 400 -t 10 -p 5 -f 80 -d 0 -a 255 -i 3 -s 3  -c 22
 *
 */

int main ( int argc, char *argv[] )
{

    QApplication a ( argc, argv );

    QCommandLineOption szeles_opt ( {"w","szelesseg"}, "Oszlopok (cellakban) szama.", "szelesseg", "200" );
    QCommandLineOption magas_opt ( {"m","magassag"}, "Sorok (cellakban) szama.", "magassag", "150" );
    QCommandLineOption hangyaszam_opt ( {"n","hangyaszam"}, "Hangyak szama.", "hangyaszam", "100" );
    QCommandLineOption sebesseg_opt ( {"t","sebesseg"}, "2 lepes kozotti ido (millisec-ben).", "sebesseg", "100" );
    QCommandLineOption parolgas_opt ( {"p","parolgas"}, "A parolgas erteke.", "parolgas", "8" );
    QCommandLineOption feromon_opt ( {"f","feromon"}, "A hagyott nyom erteke.", "feromon", "11" );
    QCommandLineOption szomszed_opt ( {"s","szomszed"}, "A hagyott nyom erteke a szomszedokban.", "szomszed", "3" );
    QCommandLineOption alapertek_opt ( {"d","alapertek"}, "Indulo ertek a cellakban.", "alapertek", "1" );
    QCommandLineOption maxcella_opt ( {"a","maxcella"}, "Cella max erteke.", "maxcella", "50" );
    QCommandLineOption mincella_opt ( {"i","mincella"}, "Cella min erteke.", "mincella", "2" );
    QCommandLineOption cellamerete_opt ( {"c","cellameret"}, "Hany hangya fer egy cellaba.", "cellameret", "4" );
    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption ( szeles_opt );
    parser.addOption ( magas_opt );
    parser.addOption ( hangyaszam_opt );
    parser.addOption ( sebesseg_opt );
    parser.addOption ( parolgas_opt );
    parser.addOption ( feromon_opt );
    parser.addOption ( szomszed_opt );
    parser.addOption ( alapertek_opt );
    parser.addOption ( maxcella_opt );
    parser.addOption ( mincella_opt );
    parser.addOption ( cellamerete_opt );

    parser.process ( a );

    QString szeles = parser.value ( szeles_opt );
    QString magas = parser.value ( magas_opt );
    QString n = parser.value ( hangyaszam_opt );
    QString t = parser.value ( sebesseg_opt );
    QString parolgas = parser.value ( parolgas_opt );
    QString feromon = parser.value ( feromon_opt );
    QString szomszed = parser.value ( szomszed_opt );
    QString alapertek = parser.value ( alapertek_opt );
    QString maxcella = parser.value ( maxcella_opt );
    QString mincella = parser.value ( mincella_opt );
    QString cellameret = parser.value ( cellamerete_opt );

    qsrand ( QDateTime::currentMSecsSinceEpoch() );

    AntWin w ( szeles.toInt(), magas.toInt(), t.toInt(), n.toInt(), feromon.toInt(), szomszed.toInt(), parolgas.toInt(),
                  alapertek.toInt(), mincella.toInt(), maxcella.toInt(),
                  cellameret.toInt() );

    w.show();

    return a.exec();
}
              
          ]]></programlisting>  
          Példányosítjuk a QApplication osztályt, látható, hogy a konstruktor paraméterei a főprogram paraméterei(argumentumai) lesznek.
          A lehetséges kapcsolókat felsoroljuk, létrehozzuk a parsert.
          A QString osztálybeli QStringeknek "értékül adjuk" az argumentumok értékeit, a show függvénnyel megjelenítünk, majd végrehajtunk az exec függvény segítségével, 
          ezt már csak a Q billentyű, vagy az ablak bezárása állítja le.    
        </para>    
        <para>
        </para>    
                     
    </section>        
    <section>
        <title>Java életjáték  (passz) </title>
        <para>
            Írd meg Java-ban a John Horton Conway-féle életjátékot, 
            valósítsa meg a sikló-kilövőt!
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>   
 
    </section>        
    <section>
        <title>Qt C++ életjáték</title>
        <para>
            Most Qt C++-ban!
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/pocsimate/bhax/tree/master/thematic_tutorials/bhax_textbook/source/7.fejezet/QT%20eletjatek">https://gitlab.com/pocsimate/bhax/tree/master/thematic_tutorials/bhax_textbook/source/7.fejezet/QT%20eletjatek</link>               
        </para>
        <para>
            Tutor: <link xlink:href="https://gitlab.com/bacsikmatyas/bhax/blob/master">Bacsik Mátyás</link>
        </para>

        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        
        <para>
        Conway -akiről a feladatcsokor is kapta a nevét- az 1970-es években
            saját sejtautomatát alkotott, amely igencsak megosztó lett, felháborodást is keltett.
             Lényegében egy szemlélődős játékról van szó, ahol feltételeket alkotunk
             arról, hogy a kreált lényeink hogyan maradhatnak életben, hogyan születhetnek, illetve halálozhatnak
            el. Ezt követően már csak megfigyelés.
            A híres matematikusunk 3 szabályt alkotott, mégis elérte, hogy napjainkig is 
            ez a legnépszerűbb sejtautomata. A szabályok:
        <programlisting>
1.: Egy sejt akkor és csak akkor marad életben, ha 2, vagy 3 élő szomszédja is van.
2.: Ha egy sejtnek 3-nál több szomszédja van, túlnépesedés miatt hal meg, ha pedig 2-nél kevesebb szomszédainak száma, akkor elszigetelődés miatt hal meg.
3.: Sejt akkor születik, ha az adott üres cella szomszédos celláiban 3 élő sejt található.
            </programlisting>
        </para>
        <para>
            Hogyan használd:
            <screen>
sudo apt-get install libqt4-dev
qmake Sejtauto.pro
make
./Sejtauto
            </screen>
        </para>
        <para>
        Futás közben:
            
        <mediaobject>
          <imageobject>
            <imagedata fileref="source/sejauto.png" format="PNG"/>
          </imageobject>
        </mediaobject>    
            
        </para>
        <para>
            Kezdjük a <filename>sejtszal.h</filename> header-rel.
            <programlisting language="c++"><![CDATA[
                
 #ifndef SEJTSZAL_H
#define SEJTSZAL_H

#include <QThread>
#include "sejtablak.h"

class SejtAblak;

class SejtSzal : public QThread
{
    Q_OBJECT

public:
    SejtSzal(bool ***racsok, int szelesseg, int magassag,
             int varakozas, SejtAblak *sejtAblak);
    ~SejtSzal();
    void run();

protected:
    bool ***racsok;
    int szelesseg, magassag;
    // Megmutatja melyik rács az aktuális: [rácsIndex][][]
    int racsIndex;
    // A sejttér két egymást követő t_n és t_n+1 diszkrét időpillanata
    // közötti valós idő.
    int varakozas;
    void idoFejlodes();
    int szomszedokSzama(bool **racs,
                        int sor, int oszlop, bool allapot);
    SejtAblak* sejtAblak;

};

#endif // SEJTSZAL_H               
                
            ]]></programlisting>
             Létrehozzuk a SejtSzal osztályt, amely leszármazottja lesz a QThread osztálynak.
             Publikus részében látjuk paraméterekkel a konstruktorját, a destruktorját, illetve a run függvény prototípusát.
             A protected részben látható <varname>bool ***racsok</varname> valójában egy háromdimenziós boolean típusú értékek alkotta tömb.
             Jelen vannak továbbá szélesség, illetve magasság egész típusú változók amelyek a sorok, oszlopok cellaszámának megadásában lesznek segítségünkre.
             A rácsindex változó lesz segítségünkre a rács indexelésében.
             A várakozás nevű változó fogja jelenteni a két időpillanat közti időt,
             továbbá látható az idoFejlodes, illetve a szomszedokSzama függvények prototípusai (igazából deklarációk).
             Végül létrehozunk egy sejtAblak nevű SejtAblak típusra mutató mutatót.
        </para>
        <para>
             Íme a <filename>sejtablak.h</filename> header.
            <programlisting language="c++"><![CDATA[
#ifndef SEJTABLAK_H
#define SEJTABLAK_H

#include <QMainWindow>
#include <QPainter>
#include "sejtszal.h"

class SejtSzal;

class SejtAblak : public QMainWindow
{
  Q_OBJECT
  
public:
  SejtAblak(int szelesseg = 100, int magassag = 75, QWidget *parent = 0);

  ~SejtAblak();
  // Egy sejt lehet élő
  static const bool ELO = true;
  // vagy halott
  static const bool HALOTT = false;
  void vissza(int racsIndex);
  
protected:
  // Két rácsot használunk majd, az egyik a sejttér állapotát
  // a t_n, a másik a t_n+1 időpillanatban jellemzi.
  bool ***racsok;
  // Valamelyik rácsra mutat, technikai jellegű, hogy ne kelljen a
  // [2][][]-ból az első dimenziót használni, mert vagy az egyikre
  // állítjuk, vagy a másikra.
  bool **racs;
  // Megmutatja melyik rács az aktuális: [rácsIndex][][]
  int racsIndex;
  // Pixelben egy cella adatai.
  int cellaSzelesseg;
  int cellaMagassag;
  // A sejttér nagysága, azaz hányszor hány cella van?
  int szelesseg;
  int magassag;    
  void paintEvent(QPaintEvent*);
  void siklo(bool **racs, int x, int y);
  void sikloKilovo(bool **racs, int x, int y);
  
private:
  SejtSzal* eletjatek;
  
};

#endif // SEJTABLAK_H                
                 
            ]]></programlisting>
          "Szokásosan", a QMainWindow és a QPainter osztályokat hozzáadjuk, a SejtAblak osztály a QMainWindow osztály 
          leszármazottja lesz.
          Publikus adattagjai közt található a konstruktor, a destruktor, illetve két logikai típusú konstans, ahol az élő, illetve halott sejteket definiáljuk.
          Ezek statikus változók lesznek, vagyis az osztály valamely 
          objektuma számára meg lesz osztva, és valamennyi objektum ezen
           változókat fogja használni, nem készül róluk objektumonként másolat.
           Protected-ben látható a két, számunkra fontos függvény, amelyek kirajzolják a siklót illetve a sikló kilövőt.
        </para>
        <para>
            A <filename>sejtszal.cpp</filename>
            <programlisting language="c++"><![CDATA[
 #include "sejtszal.h"

SejtSzal::SejtSzal(bool ***racsok, int szelesseg, int magassag, int varakozas, SejtAblak *sejtAblak)
{
    this->racsok = racsok;
    this->szelesseg = szelesseg;
    this->magassag = magassag;
    this->varakozas = varakozas;
    this->sejtAblak = sejtAblak;

    racsIndex = 0;
}

int SejtSzal::szomszedokSzama(bool **racs,
                              int sor, int oszlop, bool allapot) {
    int allapotuSzomszed = 0;
    // A nyolcszomszédok végigzongorázása:
    for(int i=-1; i<2; ++i)
        for(int j=-1; j<2; ++j)
            // A vizsgált sejtet magát kihagyva:
            if(!((i==0) && (j==0))) {
        // A sejttérbõl szélének szomszédai
        // a szembe oldalakon ("periódikus határfeltétel")
        int o = oszlop + j;
        if(o < 0)
            o = szelesseg-1;
        else if(o >= szelesseg)
            o = 0;

        int s = sor + i;
        if(s < 0)
            s = magassag-1;
        else if(s >= magassag)
            s = 0;

        if(racs[s][o] == allapot)
            ++allapotuSzomszed;
    }

    return allapotuSzomszed;
}

void SejtSzal::idoFejlodes() {

    bool **racsElotte = racsok[racsIndex];
    bool **racsUtana = racsok[(racsIndex+1)%2];

    for(int i=0; i<magassag; ++i) { // sorok
        for(int j=0; j<szelesseg; ++j) { // oszlopok

            int elok = szomszedokSzama(racsElotte, i, j, SejtAblak::ELO);

            if(racsElotte[i][j] == SejtAblak::ELO) {
                /* Élõ élõ marad, ha kettõ vagy három élõ
             szomszedja van, különben halott lesz. */
                if(elok==2 || elok==3)
                    racsUtana[i][j] = SejtAblak::ELO;
                else
                    racsUtana[i][j] = SejtAblak::HALOTT;
            }  else {
                /* Halott halott marad, ha három élõ
             szomszedja van, különben élõ lesz. */
                if(elok==3)
                    racsUtana[i][j] = SejtAblak::ELO;
                else
                    racsUtana[i][j] = SejtAblak::HALOTT;
            }
        }
    }
    racsIndex = (racsIndex+1)%2;
}


/** A sejttér idõbeli fejlõdése. */
void SejtSzal::run()
{
    while(true) {
        QThread::msleep(varakozas);
        idoFejlodes();
        sejtAblak->vissza(racsIndex);
    }

}

SejtSzal::~SejtSzal()
{
}
            ]]></programlisting>
            A konstruktor beállítja a megfelelő értékeket, nullázza a racsIndex változót.
            A szomszedokSzama() függvény annyit csinál hogy a megfelelő állapotú (élő/halott) sejtek számát visszaadja 
            az adott sejt környezetében, ez 8 elemet érinthet, mivel 8 sejt veszi körbe.
            Az idoFejlodes() függvény a játékszabályok, illetve az aktuális rács állapota alapján kialakítja a következő időpillanatbeli állapotot a másik rácsba.
            A run() függvényben található végtelen ciklus altatja a szálat a várakozás függvényében, amely két időpillanat közt
            eltelt idő, majd hívja az idoFejlodes() függvényt, a vissza() függvény segítségével pedig újrarajzol a megadott rácspont alapján.
            Utolsó sorában a konstruktor szerepel.
        </para>
        <para>
            A <filename>sejtablak.cpp</filename>
            <programlisting language="c++"><![CDATA[
#include "sejtablak.h"

SejtAblak::SejtAblak(int szelesseg, int magassag, QWidget *parent)
: QMainWindow(parent)
{
  setWindowTitle("A John Horton Conway-féle életjáték");
  
  this->magassag = magassag;
  this->szelesseg = szelesseg;

  
  cellaSzelesseg = 6;
  cellaMagassag = 6;

  setFixedSize(QSize(szelesseg*cellaSzelesseg, magassag*cellaMagassag));
  
  racsok = new bool**[2];
  racsok[0] = new bool*[magassag];
  for(int i=0; i<magassag; ++i)
    racsok[0][i] = new bool [szelesseg];
  racsok[1] = new bool*[magassag];
  for(int i=0; i<magassag; ++i)
    racsok[1][i] = new bool [szelesseg];

  racsIndex = 0;
  racs = racsok[racsIndex];

  // A kiinduló racs minden cellája HALOTT
  for(int i=0; i<magassag; ++i)
    for(int j=0; j<szelesseg; ++j)
      racs[i][j] = HALOTT;
    // A kiinduló racsra "ELOlényeket" helyezünk
    //siklo(racs, 2, 2);

    sikloKilovo(racs, 5, 60);

  eletjatek = new SejtSzal(racsok, szelesseg, magassag, 120, this);

  eletjatek->start();
  
}

void SejtAblak::paintEvent(QPaintEvent*) {
  QPainter qpainter(this);
  
  // Az aktuális
  bool **racs = racsok[racsIndex];
  // racsot rajzoljuk ki:
  for(int i=0; i<magassag; ++i) { // végig lépked a sorokon
    for(int j=0; j<szelesseg; ++j) { // s az oszlopok
      // Sejt cella kirajzolása
      if(racs[i][j] == ELO)
	qpainter.fillRect(j*cellaSzelesseg, i*cellaMagassag,
			  cellaSzelesseg, cellaMagassag, Qt::black);
	else
	  qpainter.fillRect(j*cellaSzelesseg, i*cellaMagassag,
			    cellaSzelesseg, cellaMagassag, Qt::white);
	  qpainter.setPen(QPen(Qt::gray, 1));
	
	qpainter.drawRect(j*cellaSzelesseg, i*cellaMagassag,
			  cellaSzelesseg, cellaMagassag);
    }
  }
  
  qpainter.end();
}


SejtAblak::~SejtAblak()
{
  delete eletjatek;
  
  for(int i=0; i<magassag; ++i) {
    delete[] racsok[0][i];
    delete[] racsok[1][i];
  }
  
  delete[] racsok[0];
  delete[] racsok[1];
  delete[] racsok;
  
  
}

void SejtAblak::vissza(int racsIndex)
{
  this->racsIndex = racsIndex;
  update();
}

void SejtAblak::siklo(bool **racs, int x, int y) {
  
  racs[y+ 0][x+ 2] = ELO;
  racs[y+ 1][x+ 1] = ELO;
  racs[y+ 2][x+ 1] = ELO;
  racs[y+ 2][x+ 2] = ELO;
  racs[y+ 2][x+ 3] = ELO;
  
}

void SejtAblak::sikloKilovo(bool **racs, int x, int y) {
  
  racs[y+ 6][x+ 0] = ELO;
  racs[y+ 6][x+ 1] = ELO;
  racs[y+ 7][x+ 0] = ELO;
  racs[y+ 7][x+ 1] = ELO;
  
  racs[y+ 3][x+ 13] = ELO;
  
  racs[y+ 4][x+ 12] = ELO;
  racs[y+ 4][x+ 14] = ELO;
  
  racs[y+ 5][x+ 11] = ELO;
  racs[y+ 5][x+ 15] = ELO;
  racs[y+ 5][x+ 16] = ELO;
  racs[y+ 5][x+ 25] = ELO;
  
  racs[y+ 6][x+ 11] = ELO;
  racs[y+ 6][x+ 15] = ELO;
  racs[y+ 6][x+ 16] = ELO;
  racs[y+ 6][x+ 22] = ELO;
  racs[y+ 6][x+ 23] = ELO;
  racs[y+ 6][x+ 24] = ELO;
  racs[y+ 6][x+ 25] = ELO;
  
  racs[y+ 7][x+ 11] = ELO;
  racs[y+ 7][x+ 15] = ELO;
  racs[y+ 7][x+ 16] = ELO;
  racs[y+ 7][x+ 21] = ELO;
  racs[y+ 7][x+ 22] = ELO;
  racs[y+ 7][x+ 23] = ELO;
  racs[y+ 7][x+ 24] = ELO;
  
  racs[y+ 8][x+ 12] = ELO;
  racs[y+ 8][x+ 14] = ELO;
  racs[y+ 8][x+ 21] = ELO;
  racs[y+ 8][x+ 24] = ELO;
  racs[y+ 8][x+ 34] = ELO;
  racs[y+ 8][x+ 35] = ELO;
  
  racs[y+ 9][x+ 13] = ELO;
  racs[y+ 9][x+ 21] = ELO;
  racs[y+ 9][x+ 22] = ELO;
  racs[y+ 9][x+ 23] = ELO;
  racs[y+ 9][x+ 24] = ELO;
  racs[y+ 9][x+ 34] = ELO;
  racs[y+ 9][x+ 35] = ELO;
  
  racs[y+ 10][x+ 22] = ELO;
  racs[y+ 10][x+ 23] = ELO;
  racs[y+ 10][x+ 24] = ELO;
  racs[y+ 10][x+ 25] = ELO;
  
  racs[y+ 11][x+ 25] = ELO;
  
}                
        ]]></programlisting>
        Előállítjuk az fix méretű ablakot a paraméterekből,
         illetve az ablak nevét is megadjuk.
         2 rácsot hozunk létre, majd a rácsokat HALOTT sejtekkel töltjük fel.
         Elhelyezünk egy siklókilövőt a rácsban.
         Létrehozunk egy Sejtszál objektumot a megfelelő paraméterekkel, 
         meghívjuk a start() függvényt, ez fogja meghívni a SejtSzal run() függvényét
          amely hatására elindul az életjáték.
          A paintEvent() fogja kirajzolni a sejteket, amennyiben élő a sejt,
          feketére, egyébként fehérre állítja a tollat és rajzol egy megfelelő színű négyzetet.
          A vissza() függvényt már láttuk, hogy a run() hívja, sajátjára állítja a paraméterként kapott racsIndex-et, majd
          újrarajzolja a képernyőt.
          A destruktor törli a szálat, a rácsok tartalmát, a rácsokat, illetve a rácsokat tartalmazó tömböt.
          A siklo(), valamit sikloKilovo() metódusok funkcióját már említettem, a rácsban x és y koordinátákból
           indulva valósítják meg az élőlényt.
        </para> 
        <para>
            A <filename>main</filename>:
         <programlisting language="c++"><![CDATA[   
 #include <QApplication>
#include "sejtablak.h"
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  SejtAblak w(100, 75);
  w.show();
  
  return a.exec();
}
        
         ]]></programlisting>   
         Itt már lényegében csak példányosítunk, a SejtAblak példányosításával elindult
         a játék, meghívjuk rá a már többször is említett show() függvényt,
          visszatérési értékként fog szerepelni most is a végrehajtás, amely
           0 visszatérési értékkel akkor fog visszatérni, amennyiben bezárul az ablak.
        </para>           
    </section>        
    <section>
        <title>BrainB Benchmark (passz)</title>
        <para>
        </para>
        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
        <para>
Használat előtt, ha még nem tettük meg, telepítsük a szükséges eszközöket: sudo apt-get install libqt4-dev--
sudo apt-get install opencv-data--
sudo apt-get install libopencv-dev

</para>
<para>
A BrainB Benchmark esport tehetségek felkutatásával foglalkozik. Az agy kognitív képességeit vizsgálja, olyan feladattal, hogy utána a szerzett pont összemérhető legyen az emberek között. Lényegében a karakterelvesztést teszteli, azaz ha elveszítjük a karakterünket, mennyi idő elteltével találjuk meg, illetve ha megvan, meddig tart elveszteni.

</para>
<para>
A játék összesen 10 percig tart, és minél bonyolultabb kép lesz a végén, annál jobb vagy. A végén az eredmény egy fájlban lesz. A futtatás a már látott módon. Fontos, hogy egy mappában legyenek a szükséges fájlok. Kilépni menet közben is lehet, az Esc-el. Az addigi eredményeknek is meglesz a leírása. Fordítás, futtatás a következőképpen: 
          
            <function>qmake BrainB.pro--</function>, <function>make--</function>,
            <function>./BrainB--</function>. 

</para>

        <para>
            <programlisting language="c++"><![CDATA[
#include <QApplication>
#include <QTextStream>
#include <QtWidgets>
#include "BrainBWin.h"

int main ( int argc, char **argv )
{
        QApplication app ( argc, argv );

        QTextStream qout ( stdout );
        qout.setCodec ( "UTF-8" );
            
                qout << "\n" << BrainBWin::appName << QString::fromUtf8 ( " Copyright (C) 2017, 2018 Norbert Bátfai" ) << endl;
                QRect rect = QApplication::desktop()->availableGeometry();
           
                BrainBWin brainBWin ( rect.width(), rect.height() );
           
BrainBWin::BrainBWin ( int w, int h, QWidget *parent ) : QMainWindow ( parent )
{


        statDir = appName + " " + appVersion + " - " + QDate::currentDate().toString() + QString::number ( QDateTime::currentMSecsSinceEpoch() );

        brainBThread = new BrainBThread ( w, h - yshift );
        brainBThread->start();

        connect ( brainBThread, SIGNAL ( heroesChanged ( QImage, int, int ) ),
                  this, SLOT ( updateHeroes ( QImage, int, int ) ) );

        connect ( brainBThread, SIGNAL ( endAndStats ( int ) ),
                  this, SLOT ( endAndStats ( int ) ) );

}

BrainBThread::BrainBThread ( int w, int h )
{

        dispShift = heroRectSize+heroRectSize/2;

        this->w = w - 3 * heroRectSize;
        this->h = h - 3 * heroRectSize;

        std::srand ( std::time ( 0 ) );

        Hero me ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                  this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                                                         255.0 * std::rand() / ( RAND_MAX + 1.0 ), 9 );

        Hero other1 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 5, "Norbi Entropy" );
        Hero other2 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 3, "Greta Entropy" );
        Hero other4 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 5, "Nandi Entropy" );
        Hero other5 ( this->w / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100,
                      this->h / 2 + 200.0 * std::rand() / ( RAND_MAX + 1.0 ) - 100, 255.0 * std::rand() / ( RAND_MAX + 1.0 ), 7, "Matyi Entropy" );

        heroes.push_back ( me );
        heroes.push_back ( other1 );
        heroes.push_back ( other2 );
        heroes.push_back ( other4 );
        heroes.push_back ( other5 );

}
Hero ( int x=0, int  y=0, int color=0, int agility=1,  std::string name ="Samu Entropy" )
    void move ( int maxx, int maxy, int env ) {

        int newx = x+ ( ( ( double ) agility*1.0 ) * ( double ) ( std::rand() / ( RAND_MAX+1.0 ) )-agility/2 ) ;
        if ( newx-env > 0 && newx+env < maxx ) {
            x = newx;
        }
        int newy = y+ ( ( ( double ) agility*1.0 ) * ( double ) ( std::rand() / ( RAND_MAX+1.0 ) )-agility/2 );
        if ( newy-env > 0 && newy+env < maxy ) {
            y = newy;
        }



class BrainBThread : public QThread
{
    Q_OBJECT

    
     //Norbi
    cv::Scalar cBg { 247, 223, 208 };
    cv::Scalar cBorderAndText { 47, 8, 4 };
    cv::Scalar cCenter { 170, 18, 1 };
    cv::Scalar cBoxes { 10, 235, 252 };
    Heroes heroes;
    int heroRectSize {40};

    cv::Mat prev {3*heroRectSize, 3*heroRectSize, CV_8UC3, cBg };
    int bps;
    long time {0};
    long endTime {10*60*10};
    int delay {100};

    bool paused {true};
    int nofPaused {0};

    std::vector<int> lostBPS;
    std::vector<int> foundBPS;

    int w;
    int h;
    int dispShift {40};]]>
            </programlisting>

        </para>
        <para>
        </para>
        <para>
        </para>
        <para>
        </para>
        <para>
        </para>
        <para>
        </para>
        <para>
        </para>            
    </section>        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
