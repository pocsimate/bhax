#include <iostream>

class Rectangle{

public:
    int width;
    int height;
    

    virtual void setWidth(int width){
        this->width = width;
    }
    
    virtual void setHeight(int height){
        this->height = height;
    }
    
    int getWidth(){
        return width;
    }
    
    int getHeight(){
        return height;
    }
    
    int getArea(){
        return width * height;
    }
};
class Square : public Rectangle{

public:
    void setWidth(int width){
        this->width = width;
        this->height = width;
    }
    
    void setHeight(int height){
        this->width = height;
        this->height = height;
    }
    
};

    int main (){

        Rectangle* r = new Square();
        r->setWidth(2);
        r->setHeight(7);
        std::cout << r->getArea();
}
