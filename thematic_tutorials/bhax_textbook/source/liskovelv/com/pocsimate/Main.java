
public class Main {

    public static void main(String[] args) {
/*
        Rectangle rec = new Rectangle(6, 15);
        System.out.println(rec.getHeight() + ", " + rec.getWidth());
*/

        Square sq = new Square(6);
        System.out.println(sq.getWidth() + ", " + sq.getHeight() + ", " + sq.getArea());

        Square sq2 = new Square();
        sq2.setHeight(5);
        sq2.setWidth(10);
        System.out.println(sq2.getWidth() + ", " + sq2.getHeight() + ", " + sq2.getArea());


       /* System.out.println(sq instanceof Square);
        System.out.println(rec instanceof Square);
        System.out.println(sq instanceof Rectangle);
        */
        Rectangle rec2 = new Square();
        rec2.setHeight(5);
        rec2.setWidth(9);
        System.out.println(rec2.getArea());

    }
}
