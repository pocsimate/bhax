#include <iostream>
#include <vector>

class Test{
    
private:
    int id;
    std::string name;

public:

   Test(): id(0), name(""){ std:: cout << "0param  const" << std::endl;}
   Test(int id, std::string name): id(id), name(name){std:: cout << "param const" << std::endl;}
   Test(const Test &other){
       std::cout << "copy constructor" << std::endl;
       id = other.id;
       name = other.name;
   }
   ~Test(){
       std::cout << "destructor" << std::endl;
   }

    const Test &operator=(const Test &other){
        std::cout << "copy assignment" << std::endl;
        id = other.id;
        name = other.name;

        return *this;
    }

   void print(){
       std::cout << id << ", " << name << std::endl;
   }
};

int main(){
    Test t(2, "?");

    std::vector<Test>vectest;
    vectest.push_back(t);

    vectest[0].print();
}
