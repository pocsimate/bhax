public class PolarGen {

    boolean nincsTarolt = true;
    double tarolt;

    public PolarGen() {

        nincsTarolt = true;
    }

    public double kovetkezo() {
        if (nincsTarolt) {
            double u1, u2, v1, v2, s;
            do {

                u1 = Math.random();
                u2 = Math.random();

                v1 = 2 * u1 - 1;
                v2 = 2 * u2 - 1;

                s = v1 * v1 + v2 * v2;


            } while (s > 1);

            double x = Math.sqrt((-2 * Math.log(s)) / s);
            tarolt = x * v2;
	    nincsTarolt = !nincsTarolt;

            return x * v1;

        } else {
            nincsTarolt = !nincsTarolt;
            return tarolt;
        }
    }


    public static void main(String[] args) {

        PolarGen pg = new PolarGen();

        for (int i = 0; i < 10; i++)
            System.out.println(i+1 + ". : " + pg.kovetkezo());
    }
}

