#include <iostream>

void tounar(int a){
    for(int i=0; i<a; i++)
        std::cout << "1";
    
    std::cout << std::endl;
}

int main(){
    
    int val;
    std::cout << "Type a number in decimal." << std::endl;
    while(std::cin >> val){
        tounar(val);
    }
    
    return 0;
}
