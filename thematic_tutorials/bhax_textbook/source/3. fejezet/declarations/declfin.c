#include <stdio.h>

int *elso(void);
int *masodik(void);
int *(*elsovagymasodik (int ertek))(void);

int main(){
    
    int egesz=5;
    int egesz2=10;
    

    int* egeszpt=&egesz;
	printf("int: %d  ,  pt: %d\n", egesz,  *egeszpt);
    

    int &egeszref=egesz;
    printf("egesz: %d  ,  egeszref: %d\n", egesz,  egeszref);
    
    ++egeszref;
    printf("egesz: %d  ,  egeszref: %d\n", egesz,  egeszref);
    
    egeszref=egesz2;
    printf("egesz: %d  ,  egeszref: %d\n", egesz,  egeszref);
    
    egesz=5;
    egesz2=10;
    

    int egesztomb[5]={1,2,3,4,5};
    
    for (int i=0; i<5; i++)
        printf("A tömb %d. eleme: %d\n", i+1, egesztomb[i]);
    

    int (&reft)[5]=egesztomb;
    
    for (int i=0; i<5 ; ++i)
        printf("%d\n", reft[i]);
    

    int* muttomb[5];
    
    for (int i=0; i<5 ; ++i)
        muttomb[i] = &egesztomb[i];
    
    for (int i=0; i<5; i++)
        printf("A mutatótömb %d. eleme: %d\n", i+1, *(muttomb[i]));
    

    int* mutt=elso();
        printf("%d\n", *mutt);
    
        
    int *(*fgv)(void);
    
       fgv=masodik;
       int *em=fgv();
        
       printf("hehe %d\n", *fgv());
       
       
       
    int *(*(*fn)(int))(void);
        fn = elsovagymasodik;
        
        fgv = *fn (1);
        printf("whoa %d\n", *fgv());
    

    return 0;
}

int *elso(void){
    
    int x = 5;
    int *p = &x;
    
    *p *= *p;
    
  return p;
    
}

int *masodik(void){
   
    int x = 5;
    int *p = &x;
    
    *p += *p;
    
  return p;
    
}

int *(*elsovagymasodik (int ertek))(void){
    
    switch(ertek){
        case 1: return elso;
        break;
        
        case 0: return masodik;
        break;
        
    }
    
}
