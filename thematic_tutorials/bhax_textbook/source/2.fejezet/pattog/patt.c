

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void gotoxy(int x, int y)                     
{
  int i;
  for(i=0; i<y; i++) printf("\n");             
  for(i=0; i<x; i++) printf(" ");             
  printf("o\n");    //labda ikonja

}


int main()
{
  
  int egyx=1;
  int egyy=-1;
  int i;
  int x=10;   //a labda kezdeti pozíciója
  int y=20;
  int ty[23];//magasság     
  int tx[80];//szélesség

  //pálya szélei 

  for(i=0; i<23; i++)
       ty[i]=1;

  ty[1]=-1;
  ty[22]=-1;

  for(i=0; i<79; i++)
       tx[i]=1;

  tx[1]=-1;
  tx[79]=-1;



  for(;;)
  {

    gotoxy(x,y);
    //printf("o\n"); Áthelyezve a gotoxy függvényve

    x+=egyx;
    y+=egyy;

    egyx*=tx[x];
    egyy*=ty[y];

    usleep (200000);
    system("clear");
  }

}
