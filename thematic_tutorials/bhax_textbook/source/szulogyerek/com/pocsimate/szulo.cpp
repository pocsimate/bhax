#include <iostream>

class Szulo {
public:
    
    Szulo(){
    }
    
    Szulo(std::string name, int age){
        this->name = name;
        this->age = age;
    }
    
    std::string getName(){
        return name;
    }
    
    void setName(std::string name){
        this->name = name;
    }
    
    void setAge(int age){
        this->age = age;
    }
    
protected:
    std::string name;
    int age;
};

class Gyerek : public Szulo{  
public:
    Gyerek(){
    }
    
    Gyerek(std::string name, int age){
        this->name = name;
        this->age = age;
    }
    
     int getAge(){
        return age;
    }
};

int main(){

    Szulo* s = new Gyerek("Szabolcs", 45);
    std::cout << s->getAge() << " " << s->getName() << std::endl;

    return 0;
}
