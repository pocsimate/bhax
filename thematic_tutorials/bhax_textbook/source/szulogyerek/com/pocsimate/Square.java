
public class Square extends Rectangle {

    public Square(int height) {
        super(height, height);
    }

    public int getArea() {
        return height * width;
    }
}
