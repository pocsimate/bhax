#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>
#include <map>
using namespace std;

vector<pair<string, int>> sortByValue ( map <string, int> &rank )
{ 
    vector<pair<string, int>> sorted;
    for ( auto & i : rank ) {

        if ( i.second ) {
        pair<string, int> p {i.first, i.second};
        sorted.push_back ( p );
        }
}
    sort (
          begin ( sorted ), end ( sorted ),
          [ = ] ( auto && p1, auto && p2 ) {
    return p1.second > p2.second;
}

);
    return sorted;
}

int main() {
    map<string, int> mymap;
    
    mymap.insert(pair<string, int>("Hali", 40)); 
    mymap.insert(pair<string, int>("Ciao", 30)); 
    mymap.insert(pair<string, int>("Bonjour", 60)); 
    mymap.insert(pair<string, int>("Hello", 20)); 
    
    map<string, int>::iterator itr; 
    cout << "\nThe map mymap is : \n"; 
    cout << "\tKEY\tELEMENT\n"; 
    for (itr = mymap.begin(); itr != mymap.end(); ++itr) { 
        cout << '\t' << itr->first 
             << '\t' << itr->second << '\n'; 
    } 
    
    vector<pair<string, int>> sorted (sortByValue(mymap));
    cout << "\nThe vector sorted is : \n"; 
    cout << "\tKEY\tELEMENT\n";  
    for(auto const& i:sorted){
       cout << '\t' << i.first
            << '\t' << i.second << endl;   
    } 
}