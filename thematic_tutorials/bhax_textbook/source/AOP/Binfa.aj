privileged aspect BinfaAspect {
 void around(char b, LZWBinFa f):
  target(f) && call(void egyBitFeldolg(char)) && args(b) {
   if (b == '0') {
    if (f.fa.nullasGyermek() == null) {
     LZWBinFa.Csomopont uj = f.new Csomopont('0');
     f.fa.ujNullasGyermek(uj);
     f.fa = f.gyoker;
     f.egyBitFeldolg('0');
    } else {
     f.fa = f.fa.nullasGyermek();
    }
   } else {
    if (f.fa.egyesGyermek() == null) {
     LZWBinFa.Csomopont uj = f.new Csomopont('1');
     f.fa.ujEgyesGyermek(uj);
     f.fa = f.gyoker;
     f.egyBitFeldolg('1');
    } else {
     f.fa = f.fa.egyesGyermek();
    }
   }
  }
}
